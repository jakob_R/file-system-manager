﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace ConsoleApp1
{
    class FileService
    {       

        public static string resourcesPath = @"../../Resources/";
        
        //Gets all filePaths and lists the file names 
        public void ListAllFileNames()
        {
            Console.WriteLine("\nListing all filenames: \n");
            try
            {

            } catch (Exception e)
            {
                Console.WriteLine(e);
            }

            List<String> fileNames = GetAllFilePaths();
            foreach (String file in fileNames) { Console.WriteLine(Path.GetFileName(file)); }
        }      


        //Lists files by extension
        public void ListFilesByExtension()
        {
            List<string> fileNames = GetAllFilePaths();

            // lists extensions
            Console.WriteLine("\nListing all extensions: \n");
            List<string> extensions = ListExtensions(fileNames); 
            

            //Prompts user for extension
            Console.WriteLine("\nEnter extension to get specific files by: ");
            string input = Console.ReadLine();
            if (!extensions.Contains(input))
            {
                Console.WriteLine("There are no files with that extension!\n");
                return;
            }

            //Lists files with selected extension
            Console.WriteLine($"\nFiles with extension {input}:");
            foreach (string file in fileNames)
            {
                if (Path.GetExtension(file) == input)
                {
                    Console.WriteLine(Path.GetFileName(file));
                }
            }
            return;
        }
        public void ManipulateTextFile()
        {
            Console.WriteLine("\nEntering File Manipulator... \n");

            FileManipulator filemanipulator = new FileManipulator();
            Console.WriteLine("\n==============================\n");
            Console.WriteLine("<===== File Manipulator =====>");
            Console.WriteLine("\n==============================\n");

            filemanipulator.DisplayFileManipulator();
        }

        //Gets all filepaths in resources folder
        private static List<String> GetAllFilePaths()
        {
            List<String> fileNames = new List<String>(Directory.GetFiles(resourcesPath));
            return fileNames;
        }

        //Lists each unique file extension in the given filepaths list and returns them
        private static List<string> ListExtensions(List<string> fileNames)
        {
            List<string> extensions = new List<string>();
            foreach (string file in fileNames)
            {

                if (!extensions.Contains(Path.GetExtension(file)))
                {
                    Console.WriteLine(Path.GetExtension(file));
                    extensions.Add(Path.GetExtension(file));
                }
            }
            return extensions;
        }
    }

    class FileManipulator
    {
        string[] options = new string[] { "1. Display file name", "2. Display file Size", "3. Display line count of file ", "4. Search for a word", "5. Exit File Manipulator" };
        string filePath = @"../../Resources/Dracula.txt";

        public void DisplayFileManipulator()
        {
            DisplayFileManipulatorMenu();             
        }

        //Displays menu and handles input
        public void DisplayFileManipulatorMenu()
        {
            int selectedOption;
            try
            {
                do
                {
                    DisplayOptions();
                    string input = Console.ReadLine();
                    selectedOption = Int32.Parse(input);
                    switch (selectedOption)
                    {
                        case 1:
                            DisplayNameOfFile();
                            break;
                        case 2:
                            DisplaySizeOfFile();
                            break;
                        case 3:
                            DisplayLinesOfFile();
                            break;
                        case 4:
                            SearchForWord();
                            break;                       
                        case 5:
                            Console.WriteLine("Exiting file manipulator");
                            return;
                        default:
                            Console.WriteLine("Not a valid option try again!");
                            break;
                    }


                } while (selectedOption != 5);
            }
            catch (FileNotFoundException fnfe)
            {
                Console.WriteLine("Could not find txt file");
            }
            catch (Exception ex)
            {
                Console.WriteLine("\n Invalid option! \n Exiting file manipulator...");
            }
        }
        
        //Displays options
        public void DisplayOptions()
                {           
                    Console.WriteLine("\nSelect an option:");
                    foreach (string option in options)
                    {
                        Console.WriteLine(option);
                    }
                }

        //Prompts for word input and searches through the file for that word
        private void SearchForWord()
        {            
            // prompts user for search word
            string searchWord = PromptSearchWord();
            
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            int occurences = 0;
            // reads all lines in file and counts occurences of searchword
            try
            {
                string[] lines = File.ReadAllLines(filePath);
                foreach (string line in lines)
                {
                    bool contains = line.IndexOf(searchWord, StringComparison.OrdinalIgnoreCase) >= 0;
                    if (contains)
                    {
                        occurences++;
                    }
                }
            }
            catch (FileNotFoundException fnfe)
            {
                Console.WriteLine("File was not found ");
            } 
            catch (Exception e)
            {
                Console.WriteLine("error searching file");
            }
            stopWatch.Stop();
            var time = stopWatch.ElapsedMilliseconds;
            if (occurences > 0)
            {
                string logMsg = $"\nWord: {searchWord} was found {occurences} times"; 
                onTaskDone(logMsg, time);
            }
            else
            {
                string logMsg = $"\nWord: {searchWord} was not found";
                onTaskDone(logMsg, time);
            }
        }

        private string PromptSearchWord()
        {
            Console.WriteLine("\n Enter search Word");
            string input = $" {Console.ReadLine()} ";
            return input;
        }       

        public void DisplayNameOfFile()
        {
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            FileInfo fileInfo = new FileInfo(filePath);
            var fileName = fileInfo.Name;
            stopWatch.Stop();
            string logMsg = "\n Name of File: "+ fileName;
            onTaskDone(logMsg, stopWatch.ElapsedMilliseconds);
        }
        public void DisplaySizeOfFile()
        {
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            FileInfo fileInfo = new FileInfo(filePath);
            var fileLenth = fileInfo.Length;
            stopWatch.Stop();
            string logMsg = $"\n The file is: {fileInfo.Length} bytes";
            onTaskDone(logMsg, stopWatch.ElapsedMilliseconds);
        }

        public void DisplayLinesOfFile()
        {
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            var lineCount = File.ReadLines(filePath).Count();
            stopWatch.Stop();
            string logMsg = $"\n The file has: {lineCount} lines";
            onTaskDone(logMsg, stopWatch.ElapsedMilliseconds);
        }

        private void onTaskDone(string logMsg, long time)
        {
            LoggingService.LogToFile(logMsg, time);
            Console.WriteLine(logMsg);
        }
    }
}
