﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ConsoleApp1
{
    class LoggingService
    {
        public static void LogToFile(string loggText,long time)
        {
            try {
                StreamWriter streamWriter = new StreamWriter(@"../../logging/Log.txt",true);            
                streamWriter.WriteLine("\n<================= New Task Log =================>\n");
                streamWriter.WriteLine(DateTime.Now);
                streamWriter.WriteLine($"{loggText} This Task took {time} milliseconds to execute \n");
                streamWriter.Close();
            } 
            catch (Exception e) 
            { 
                Console.WriteLine(e); 
            }       

        }
    }
}
