﻿using System;
using System.Collections.Generic;

namespace ConsoleApp1
{
    class Program
    {
        //
        static string[] menuOptions = new string[] { "1. List all files in resources ", "2. List files by extension", "3. Manipulate txt file ", "4. exit" };
        static FileService fileService;
        static void Main(string[] args)
        {
            fileService = new FileService();
            displayMenu();
        }

        //Displays menu and handles input
        private static void displayMenu()
        {


            int selectedOption;
            try
            {
                do
                {
                    displayOptions();
                    string input = Console.ReadLine();
                    selectedOption = Int32.Parse(input);

                    switch (selectedOption)
                    {
                        case 1:
                            fileService.ListAllFileNames();
                            break;
                        case 2:
                            Console.WriteLine("Option 2 Selected");
                            fileService.ListFilesByExtension();
                            break;
                        case 3:
                            Console.WriteLine("Option 3 Selected");
                            fileService.ManipulateTextFile();
                            break;
                        case 4:
                            Console.WriteLine("Exiting application..");
                            break;
                        default:
                            Console.WriteLine("Not a valid option try again!");
                            break;
                    }


                } while (selectedOption != 4);
            }
            catch (Exception ex)
            {
                Console.WriteLine("\n Invalid option! \n Input must be a an integer");
                displayMenu();
            }
        }
        //Displays options
        private static void displayOptions()
        {
            Console.WriteLine("\nSelect an option:");
             foreach (string option in menuOptions )
             {
                Console.WriteLine(option);
             }
        }           
    }
}